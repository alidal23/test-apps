<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\RequestRegisterUser;
use App\Users;



class ControllerRegister extends Controller
{
    function register(RequestRegisterUser $request)
    {
        $data = $request->except(['_token','_method']);
        // dd($data);
        // $img_name = $data['image']->getClientOriginalName();
        $img_extension = $data['image']->getClientOriginalExtension();
        $currentDateTime = date("d-m-Y H i s a");
        $path = $data['image']->storeAs('public/user_images',$currentDateTime.'.'.$img_extension);
        $users = new Users;
        $users->user_first_name = $data['first_name'];
        $users->user_last_name = $data['last_name'];
        $users->user_address = $data['address'];
        $users->user_image = $path;
        if($users->save())
        {
            return view('form')->with('success','User Registered Successfully.');
        }
        else{
            return view('form')->with('failed','User Registeration Failed.');
        }
    } 
}
