<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registration</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}">
</head>
<body>
    <div class="signup-form">
        {{ Form::open(['url' => '/register', 'files' => true]) }}
        @csrf
            <h2>Register</h2>
            <hr>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        {{ Form::text('first_name',null,['class' => 'form-control', 'placeholder' => 'First Name' , 'required' => true ]) }}
                    </div>
                    <div class="col-xs-6">
                        {{ Form::text('last_name',null,['class' => 'form-control', 'placeholder' => 'Last Name' , 'required' => true ]) }}
                    </div>
                </div>        	
            </div>
            <div class="form-group">
                {{ Form::textarea('address',null,['class' => 'form-control', 'placeholder' => 'Address' , 'rows' => 3 , 'required' => true ]) }}
            </div>
            <div class="form-group">
                {{ Form::file('image',['class' => 'form-control-file' , 'required' => true ]) }}
            </div>
            <div class="form-group">
                {{ Form::submit('Sign Up',['class' => 'btn btn-primary btn-block' ]) }}
            </div>
            @if(count($errors)>0)
                @foreach ($errors->all() as $errors)
                    <p class="text-danger">{{ $errors }}</p>
                @endforeach
            @endif
            <p class="text-danger">{{ $failed ?? '' }}</p>
            <p class="text-success">{{ $success ?? '' }}</p>

        {{ Form::close() }}
    </div>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>